import express from 'express';
import Counter from './counter.mjs';
import endpoint_mw from './endpoint.mw.mjs';
import logger from './logger.mjs';

const app = express();
const counter = new Counter();
// Middleware
app.use(endpoint_mw);

// Endpoints
app.get('', (req, res) => res.send("Welcome!"));
// GET /counter-increase
app.get('/counter-increase', (req, res) => {
    // Increase count
    counter.increase();
    logger.log('info', `[COUNTER] increase ${counter.read()}`);
    res.send(`${counter.read()}`);
});
// GET /counter-read
app.get('/counter-read', (req, res) => {
    logger.log('info', `[COUNTER] read ${counter.read()}`);
    res.send(`${counter.read()}`);
});
// GET /counter-zero
app.get('/counter-zero', (req, res) => {
    // Zero counter count
    counter.zero();
    logger.log('info', `[COUNTER] zeroed ${counter.read()}`);
    res.send(`${counter.read()}`);
});

app.all('*', (req, res) => {
    res.status(404).send("Resource not found.")
});

export default app;
