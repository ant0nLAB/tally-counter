import logger from './logger.mjs';

/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    const url = req.url;
    const method = req.method
    logger.http("[ENDPOINT] " + method + " '" + url +"'");
    next();
};

export default endpoint_mw;
